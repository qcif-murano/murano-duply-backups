Murano Duply Backups
====================

This is the source for the Duply Backups Murano package running on the NeCTAR cloud.

A Makefile is included to help the build process.

You will need your NeCTAR cloud credentials loaded and the Murano CLI tools
available in your path.

You will also need the following required packages:

* [QRIScloudLib](https://bitbucket.org/qcif-murano/murano-qriscloud-lib)
